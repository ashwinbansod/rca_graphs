###############################################################################
# Script		: RCA Graph Reporting script                                  #
# Description 	: Reads the RCA data and populates the RCA reporting Graphs   #
# Author		: Ashwin Bansod	                                              #
# Date			: 05/01/2017                                                  #
###############################################################################


import pandas as pd
import numpy as np
import calendar
from datetime import datetime
from datetime import date
import matplotlib.pyplot as plt
import os

###############################################################################
# Global Constants                                                            #
###############################################################################
SUCCESS = 0
FAILURE = -1
RAW_INPUT_FILE = "RCA_Source_Data.xlsx"

DAY_INTERVAL = '1 day'
MIN_INTERVAL = '1 minute'

AGG_COUNT_IDX = 0
AGG_SUM_IDX = 1
P1 = 'P1'
P2 = 'P2'
P3 = 'P3'

TOTAL_MIN_WEEK = 10800

IMG_EXTN = ".png"
IMG_SAVE_PATH = os.path.join(os.path.expandvars("%userprofile%"), "Desktop", "RCAGraphs", "Graphs")


###############################################################################
# Classes                                                                     #
###############################################################################


# Graph formatting class to store graph formatting
class GraphFormatting:
    def __init__(self,
                 bgcolor,
                 edge_color,
                 text_color,
                 title_fontsize,
                 label_fontsize,
                 tick_fontsize,
                 bar1_color,
                 bar2_color,
                 bar3_color,
                 line1_color,
                 line2_color,
                 line3_color):
        self.bgcolor = bgcolor
        self.edge_color = edge_color
        self.text_color = text_color
        self.title_fontsize = title_fontsize
        self.label_fontsize = label_fontsize
        self.tick_fontsize = tick_fontsize
        self.bar1_color = bar1_color
        self.bar2_color = bar2_color
        self.bar3_color = bar3_color
        self.line1_color = line1_color
        self.line2_color = line2_color
        self.line3_color = line3_color


# Core Time class to store core time
class CORE_TIME:
    start_hour = 7
    start_min = 0
    start_sec = 0

    end_hour = 19
    end_min = 0
    end_sec = 0
    # end CORE_TIME class

TOTAL_CORE_MIN_WEEK = 5 * ((CORE_TIME.end_hour - CORE_TIME.start_hour) * 60 + CORE_TIME.end_min - CORE_TIME.start_min)

###############################################################################
# Functions                                                                   #
###############################################################################

# Check and create RCA Graphs directory
if not os.path.exists(IMG_SAVE_PATH):
    os.makedirs(IMG_SAVE_PATH)


# Read Excel file and populate raw data required for further computations
def readAndFormatSourceData(filename):
    raw_data = pd.read_excel(filename)
    raw_data['Total_Duration(min)'] = (raw_data['Resolution_Time'] - raw_data['Onset_Time']) / pd.Timedelta(
        MIN_INTERVAL)
    raw_data['Week_Start'] = raw_data['Onset_Time'].dt.to_period('W').apply(lambda r: pd.datetime.date(r.start_time))
    raw_data['Week_End'] = raw_data['Onset_Time'].dt.to_period('W').apply(lambda r: pd.datetime.date(r.end_time))
    raw_data['Core_Duration'] = raw_data.apply(lambda row:
                                               getCoreTimeMins(row['Onset_Time'], row['Resolution_Time']), axis=1)
    raw_data[' NonCore_Duration'] = raw_data.apply(lambda row:
                                                   row['Total_Duration(min)'] - row['Core_Duration'], axis=1)
    return raw_data


def getCoreTimeMins(curr_time, end_time):
    core_duration = 0
    today_core_start = pd.datetime(curr_time.year, curr_time.month, curr_time.day,
                                   CORE_TIME.start_hour, CORE_TIME.start_min, CORE_TIME.start_sec)
    today_core_end = pd.datetime(curr_time.year, curr_time.month, curr_time.day,
                                 CORE_TIME.end_hour, CORE_TIME.end_min, CORE_TIME.end_sec)

    while curr_time < end_time:
        next_day_core_start = today_core_start + pd.Timedelta(DAY_INTERVAL)

        # Skip Saturday & Sunday in core hours calculation (0:Monday, 6:Sunday)
        if today_core_start.weekday() >= 5:
            today_core_start = next_day_core_start
            today_core_end = pd.datetime(today_core_start.year, today_core_start.month, today_core_start.day,
                                         CORE_TIME.end_hour, CORE_TIME.end_min, CORE_TIME.end_sec)
            curr_time = next_day_core_start
            continue
        # end if

        # Check core hours
        if curr_time < today_core_start:
            curr_time = today_core_start
        elif curr_time >= today_core_start and curr_time <= today_core_end:
            if end_time < today_core_end:
                core_duration = (end_time - curr_time) / pd.Timedelta(MIN_INTERVAL)
            else:
                core_duration = (today_core_end - curr_time) / pd.Timedelta(MIN_INTERVAL)
            curr_time = today_core_end + pd.Timedelta(MIN_INTERVAL)
        elif curr_time > today_core_end:
            curr_time = next_day_core_start
            today_core_start = next_day_core_start
            today_core_end = pd.datetime(today_core_start.year, today_core_start.month, today_core_start.day,
                                         CORE_TIME.end_hour, CORE_TIME.end_min, CORE_TIME.end_sec)
            # end if
    # end while
    return core_duration


def getPivotOnWeekStart(input_df):
    by_week = input_df[['Total_Duration(min)', 'Week_Start', 'Week_End']].groupby(['Week_Start'])
    by_week_agg = by_week.aggregate(['count', 'sum'])
    return by_week_agg


def parseStartAndEndDate(start_date_str="01-1-2017", end_date_str=""):
    # convert start date and end date to date object
    start_date = datetime.strptime(start_date_str, "%m-%d-%Y").date()
    first_week_start_date = start_date - pd.Timedelta(days=start_date.weekday())
    if len(end_date_str) == 0:
        end_date = date.today()
    else:
        end_date = datetime.strptime(end_date_str, "%m-%d-%Y").date()
        if end_date > date.today():
            end_date = date.today()
    last_week_end_date = end_date - pd.Timedelta(days=end_date.weekday()) + pd.Timedelta(days=7)

    return start_date, first_week_start_date, end_date, last_week_end_date


def getLowUptimeValue(input_df, uptime_col_str):
    if len(input_df[uptime_col_str]) == 0:
        return 0

    low_uptime_val = round((int(min(input_df[uptime_col_str])) // 10) * 10, 2)
    if low_uptime_val == 100:
        low_uptime_val = 90
    return low_uptime_val


def getSLAValue(low_uptime_val):
    sla_value = 99.5
    if low_uptime_val < sla_value:
        sla_value = sla_value - low_uptime_val
    return sla_value


# Generate data for Weekly uptime stats
def generateUptimeWeeklyData(input_df, start_date_str="01-1-2017", end_date_str=""):
    # Apply groupby clause on dataframe to generate weekly stats
    by_week_agg = getPivotOnWeekStart(input_df)

    # convert start date and end date to date object
    start_date, first_week_start_date, end_date, last_week_end_date = parseStartAndEndDate(start_date_str, end_date_str)

    # create new dataframe for uptime related data
    uptime_df = pd.DataFrame()
    start_date_idx = by_week_agg.index

    # variable declaration
    week_start = []
    downtime_min = []
    uptime = []

    # Scan the data week by week 
    curr_dt = first_week_start_date
    while curr_dt < last_week_end_date:
        week_start.append(curr_dt)
        if curr_dt in start_date_idx:
            downtime_min.append(int(by_week_agg.loc[curr_dt][AGG_SUM_IDX]))
            uptime.append((TOTAL_MIN_WEEK - by_week_agg.loc[curr_dt][AGG_SUM_IDX]) * 100 / TOTAL_MIN_WEEK)
        else:
            downtime_min.append(0)
            uptime.append(100)
        curr_dt = curr_dt + pd.Timedelta(days=7)

    # create dataframe for uptime
    uptime_df['Week_Start'] = week_start
    uptime_df['Week_End'] = uptime_df['Week_Start'].apply(lambda x: x + pd.Timedelta(days=6))
    uptime_df['Week_Duration'] = uptime_df['Week_Start'].apply(lambda x: x.strftime('%d-%b') + "\n" +
                                                                         (x + pd.Timedelta(days=6)).strftime('%d-%b'))
    uptime_df['DownTime(min)'] = downtime_min
    uptime_df['Uptime(%)'] = uptime
    low_uptime_val = getLowUptimeValue(uptime_df, "Uptime(%)")
    uptime_df['Uptime_plt'] = uptime_df['Uptime(%)'].apply(lambda x: x - low_uptime_val)
    uptime_df['SLA'] = getSLAValue(low_uptime_val)

    # Compute columns for core hours
    uptime_df['Core_Duration'] = uptime_df.apply(lambda row: getWeeklyCoreDuration(row, input_df), axis=1)
    uptime_df['Core_Uptime(%)'] = uptime_df['Core_Duration'].apply(lambda x:
                                                                   (
                                                                   TOTAL_CORE_MIN_WEEK - x) * 100 / TOTAL_CORE_MIN_WEEK)
    low_core_uptime_val = getLowUptimeValue(uptime_df, "Core_Uptime(%)")
    uptime_df['Core_Uptime_plt'] = uptime_df['Core_Uptime(%)'].apply(lambda x: x - low_core_uptime_val)
    uptime_df['Core_SLA'] = getSLAValue(low_core_uptime_val)

    return uptime_df


def getCoreDuration(row, week_start, week_end):
    core_duration = 0

    if (week_start <= row['Onset_Time'].date() and row['Onset_Time'].date() <= week_end) or \
            (week_start <= row['Resolution_Time'].date() and row['Resolution_Time'].date() <= week_end) or \
            (row['Onset_Time'].date() <= week_start and week_end <= row['Resolution_Time'].date()):
        start_time = pd.datetime(week_start.year, week_start.month, week_start.day, 0, 0, 0)
        end_time = pd.datetime(week_end.year, week_end.month, week_end.day, 11, 59, 59)

        if start_time < row['Onset_Time']:
            start_time = row['Onset_Time']

        if row['Resolution_Time'] < end_time:
            end_time = row['Resolution_Time']

        core_duration = getCoreTimeMins(start_time, end_time)
    # end if
    return core_duration


def getWeeklyCoreDuration(row, input_df):
    core_duration = 0
    core_duration = input_df.apply(lambda x: getCoreDuration(x, row['Week_Start'], row['Week_End']), axis=1)
    return sum(core_duration)


# Generate weekly data for #Incidents and resolution time
def generateIncidentWeeklyData(input_df, start_date_str="01-1-2017", end_date_str=""):
    # Apply groupby clause on dataframe to generate weekly stats
    by_week_agg = getPivotOnWeekStart(input_df)
    start_date_idx = by_week_agg.index

    # convert start date and end date to date object
    start_date, first_week_start_date, end_date, last_week_end_date = parseStartAndEndDate(start_date_str, end_date_str)

    # variable declaration
    week_start = []
    downtime_min = []
    incident_count = []

    # Compute Weekly stats
    curr_dt = first_week_start_date
    while curr_dt < last_week_end_date:
        week_start.append(curr_dt)
        if curr_dt in start_date_idx:
            downtime_min.append(int(by_week_agg.loc[curr_dt][AGG_SUM_IDX]))
            incident_count.append(int(by_week_agg.loc[curr_dt][AGG_COUNT_IDX]))
        else:
            downtime_min.append(0)
            incident_count.append(0)
        curr_dt = curr_dt + pd.Timedelta(days=7)

    # create new dataframe for Incident count related data
    inc_count_df = pd.DataFrame()
    # create dataframe
    inc_count_df['Week_Start'] = week_start
    inc_count_df['Week_End'] = inc_count_df['Week_Start'].apply(lambda x: x + pd.Timedelta(days=6))
    inc_count_df['Week_Duration'] = inc_count_df['Week_Start'].apply(lambda x:
                                                                     x.strftime('%d-%b') + "\n" +
                                                                     (x + pd.Timedelta(days=6)).strftime('%d-%b'))
    inc_count_df['DownTime(min)'] = downtime_min
    inc_count_df['#Incident'] = incident_count

    return inc_count_df


# Generate weekly data for P2/P3 #Incidents and resolution time
def generateIncidentSeverityWeeklyData(input_df, start_date_str="01-1-2017", end_date_str=""):
    # Apply groupby clause on dataframe to generate weekly stats
    by_week = input_df[['Total_Duration(min)', 'Severity', 'Week_Start']].groupby(['Week_Start', 'Severity'])
    by_week_agg = by_week.aggregate(['count', 'sum'])
    start_date_idx = by_week_agg.index.levels[0]

    # convert start date and end date to date object
    start_date, first_week_start_date, end_date, last_week_end_date = parseStartAndEndDate(start_date_str, end_date_str)

    # variable declaration
    week_start = []
    p1_downtime_min = []
    p1_incident_count = []
    p2_downtime_min = []
    p2_incident_count = []
    p3_downtime_min = []
    p3_incident_count = []

    # Compute Weekly stats
    curr_dt = first_week_start_date
    while curr_dt < last_week_end_date:
        week_start.append(curr_dt)
        if curr_dt in start_date_idx:
            # Check the second row for same date, if severity is P1, update stats
            curr_dt_df = by_week_agg.loc[curr_dt]
            if P1 in curr_dt_df.index:
                p1_downtime_min.append(int(curr_dt_df.loc[P1][AGG_SUM_IDX]))
                p1_incident_count.append(int(curr_dt_df.loc[P1][AGG_COUNT_IDX]))
            else:
                p1_downtime_min.append(0)
                p1_incident_count.append(0)
            # end if
            # Check the second row for same date, if severity is P2, update stats
            if P2 in curr_dt_df.index:
                p2_downtime_min.append(int(curr_dt_df.loc[P2][AGG_SUM_IDX]))
                p2_incident_count.append(int(curr_dt_df.loc[P2][AGG_COUNT_IDX]))
            else:
                p2_downtime_min.append(0)
                p2_incident_count.append(0)
            # end if
            # Check the second row for same date, if severity is P3, update stats
            if P3 in curr_dt_df.index:
                p3_downtime_min.append(int(curr_dt_df.loc[P3][AGG_SUM_IDX]))
                p3_incident_count.append(int(curr_dt_df.loc[P3][AGG_COUNT_IDX]))
            else:
                p3_downtime_min.append(0)
                p3_incident_count.append(0)
                # end if
        else:
            p1_downtime_min.append(0)
            p1_incident_count.append(0)
            p2_downtime_min.append(0)
            p2_incident_count.append(0)
            p3_downtime_min.append(0)
            p3_incident_count.append(0)
        # end if
        curr_dt = curr_dt + pd.Timedelta(days=7)
    # end for

    # create new dataframe for Incident count related data
    inc_severity_df = pd.DataFrame()
    # create dataframe
    inc_severity_df['Week_Start'] = week_start
    inc_severity_df['Week_End'] = inc_severity_df['Week_Start'].apply(lambda x: x + pd.Timedelta(days=6))
    inc_severity_df['Week_Duration'] = inc_severity_df['Week_Start'].apply(lambda x:
                                                                           x.strftime('%d-%b') + "\n" +
                                                                           (x + pd.Timedelta(days=6)).strftime('%d-%b'))
    inc_severity_df['P1 #Incident'] = p1_incident_count
    inc_severity_df['P1 DownTime(min)'] = p1_downtime_min
    inc_severity_df['P2 #Incident'] = p2_incident_count
    inc_severity_df['P2 DownTime(min)'] = p2_downtime_min
    inc_severity_df['P3 #Incident'] = p3_incident_count
    inc_severity_df['P3 DownTime(min)'] = p3_downtime_min

    inc_severity_df['DownTime(min)'] = inc_severity_df['P1 DownTime(min)'] + \
                                       inc_severity_df['P2 DownTime(min)'] + \
                                       inc_severity_df['P3 DownTime(min)']
    inc_severity_df['#Incident'] = inc_severity_df['P1 #Incident'] + \
                                   inc_severity_df['P2 #Incident'] + \
                                   inc_severity_df['P3 #Incident']

    return inc_severity_df


def plotUptimeGraph(input_df, graph_format, title_extn=""):
    fig, ax = plt.subplots(num=None, figsize=(8, 5.5), dpi=80, facecolor=graph_format.bgcolor,
                           edgecolor=graph_format.edge_color)

    # Figure name postfix
    fig_duration_str = ""
    if len(title_extn) > 0:
        fig_duration_str = " - " + title_extn

    bar_width = 0.3
    opacity = 0.7
    yaxis_start_val = getLowUptimeValue(input_df, "Uptime(%)")

    rects = plt.bar(input_df.index, input_df['Uptime_plt'], bar_width,
                    alpha=opacity,
                    color=graph_format.bar1_color,
                    label='Uptime')

    plt.xlabel('Week Duration', fontsize=graph_format.label_fontsize)
    plt.ylabel('Uptime (%)', fontsize=graph_format.label_fontsize)

    if len(title_extn) > 0:
        title_extn = "\n" + title_extn
    ttl = plt.title('Weekly Uptime(%) Graph' + title_extn,
                    color=graph_format.text_color, fontsize=graph_format.title_fontsize)
    ttl.set_position([.5, 1.05])

    plt.xticks(input_df.index, input_df.Week_Duration, rotation=45, fontsize=graph_format.tick_fontsize)
    y_incr = (100 - yaxis_start_val) // 10
    ax.set_yticks(np.arange(0, 101 - yaxis_start_val, y_incr))
    ax.yaxis.set_ticklabels(np.arange(yaxis_start_val, 101, y_incr), fontsize=graph_format.tick_fontsize)

    # Plot SLA line 
    plt.plot(input_df.SLA, color=graph_format.line1_color, label='SLA (99.5%)')

    ax.xaxis.label.set_color(graph_format.text_color)
    ax.yaxis.label.set_color(graph_format.text_color)
    ax.tick_params(axis='x', colors=graph_format.text_color)
    ax.tick_params(axis='y', colors=graph_format.text_color)
    ax.set_facecolor(graph_format.bgcolor)
    ax.set_clip_on(False)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_color(graph_format.text_color)

    plt.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='on',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    labels = [str(round(i + yaxis_start_val, 2)) + "%" for i in input_df['Uptime_plt']]

    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center', va='bottom',
                color=graph_format.text_color)

    leg = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
    for text in leg.get_texts():
        text.set_color(graph_format.text_color)

    plt.tight_layout()
    # plt.show()

    # Save figure
    fig_name = os.path.join(IMG_SAVE_PATH, "Uptime Graph" + fig_duration_str + IMG_EXTN)
    fig.savefig(fig_name, facecolor=graph_format.bgcolor, edgecolor=graph_format.edge_color,
                bbox_extra_artists=(leg,), bbox_inches='tight')


def plotCoreUptimeGraph(input_df, graph_format, title_extn=""):
    fig, ax = plt.subplots(num=None, figsize=(8, 5.5), dpi=80, facecolor=graph_format.bgcolor,
                           edgecolor=graph_format.edge_color)

    # Figure name postfix
    fig_duration_str = ""
    if len(title_extn) > 0:
        fig_duration_str = " - " + title_extn

    bar_width = 0.3
    opacity = 0.7
    yaxis_start_val = getLowUptimeValue(input_df, "Core_Uptime(%)")

    rects = plt.bar(input_df.index, input_df['Core_Uptime_plt'], bar_width,
                    alpha=opacity,
                    color=graph_format.bar1_color,
                    label='Uptime')

    plt.xlabel('Week Duration', fontsize=graph_format.label_fontsize)
    plt.ylabel('Uptime (%)', fontsize=graph_format.label_fontsize)

    if len(title_extn) > 0:
        title_extn = "\n" + title_extn
    ttl = plt.title('Weekly Core Uptime(%) Graph' + title_extn,
                    color=graph_format.text_color, fontsize=graph_format.title_fontsize)
    ttl.set_position([.5, 1.05])

    plt.xticks(input_df.index, input_df.Week_Duration, rotation=45, fontsize=graph_format.tick_fontsize)
    y_incr = (100 - yaxis_start_val) // 10
    ax.set_yticks(np.arange(0, 101 - yaxis_start_val, y_incr))
    ax.yaxis.set_ticklabels(np.arange(yaxis_start_val, 101, y_incr), fontsize=graph_format.tick_fontsize)

    # Plot SLA line 
    plt.plot(input_df['Core_SLA'], color=graph_format.line1_color, label='SLA (99.5%)')

    ax.xaxis.label.set_color(graph_format.text_color)
    ax.yaxis.label.set_color(graph_format.text_color)
    ax.tick_params(axis='x', colors=graph_format.text_color)
    ax.tick_params(axis='y', colors=graph_format.text_color)
    ax.set_facecolor(graph_format.bgcolor)
    ax.set_clip_on(False)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_color(graph_format.text_color)

    plt.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='on',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    labels = [str(round(i + yaxis_start_val, 2)) + "%" for i in input_df['Core_Uptime_plt']]

    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center',
                va='bottom', color=graph_format.text_color)

    leg = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
    for text in leg.get_texts():
        text.set_color(graph_format.text_color)

    plt.tight_layout()
    # plt.show()

    # Save figure
    fig_name = os.path.join(IMG_SAVE_PATH, "Core Uptime Graph" + fig_duration_str + IMG_EXTN)
    fig.savefig(fig_name, facecolor=graph_format.bgcolor, edgecolor=graph_format.edge_color,
                bbox_extra_artists=(leg,), bbox_inches='tight')


def plotIncidentResolutionTimeGraph(input_df, graph_format, title_extn=""):
    fig, ax1 = plt.subplots(num=None, figsize=(8, 5.5), dpi=80, facecolor=graph_format.bgcolor,
                            edgecolor=graph_format.edge_color)

    # Figure name postfix
    fig_duration_str = ""
    if len(title_extn) > 0:
        fig_duration_str = " - " + title_extn

    bar_width = 0.3
    opacity = 0.7

    rects = plt.bar(input_df.index, input_df['DownTime(min)'], bar_width,
                    alpha=opacity,
                    color=graph_format.bar1_color,
                    label='Downtime')

    plt.xlabel('Week Duration', fontsize=graph_format.label_fontsize)
    plt.ylabel('Downtime (Hours)', fontsize=graph_format.label_fontsize)

    if len(title_extn) > 0:
        title_extn = "\n" + title_extn
    ttl = plt.title('#Incidents & Mitigation Time/week' + title_extn,
                    color=graph_format.text_color, fontsize=graph_format.title_fontsize)
    ttl.set_position([.5, 1.05])

    plt.xticks(input_df.index, input_df.Week_Duration, rotation=45, fontsize=graph_format.tick_fontsize)

    max_duration = max(input_df['DownTime(min)'])
    y_tick_increment = (((max_duration // 60) // 10) + 1) * 60
    ylables = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                              pd.Timedelta(minutes=i).components.hours,
                              pd.Timedelta(minutes=i).components.minutes)
               for i in range(0, max_duration + y_tick_increment, y_tick_increment)]
    plt.yticks(np.arange(0, max_duration + y_tick_increment, y_tick_increment), ylables,
               fontsize=graph_format.tick_fontsize)

    ax2 = ax1.twinx()
    ax2.plot(input_df['#Incident'], color=graph_format.line1_color, label="#Incidents")

    max_inc_tick_val = 6
    if max_inc_tick_val < max(input_df['#Incident']) - 1:
        max_inc_tick_val = max(input_df['#Incident']) + 1

    ax2.set_yticks(np.arange(0, max_inc_tick_val, 1))
    ax2.yaxis.label.set_color(graph_format.text_color)
    ax2.tick_params(axis='y', colors=graph_format.text_color)
    for label in ax2.xaxis.get_majorticklabels():
        label.set_fontsize(graph_format.tick_fontsize)

    col_labels = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                                 pd.Timedelta(minutes=i).components.hours,
                                 pd.Timedelta(minutes=i).components.minutes)
                  for i in input_df['DownTime(min)']]

    for rect, label in zip(rects, col_labels):
        if label == "00:00":
            continue
        height = rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center', va='bottom',
                 color=graph_format.text_color)

    ax1.xaxis.label.set_color(graph_format.text_color)
    ax1.yaxis.label.set_color(graph_format.text_color)
    ax1.tick_params(axis='x', colors=graph_format.text_color)
    ax1.tick_params(axis='y', colors=graph_format.text_color)
    ax1.set_facecolor(graph_format.bgcolor)
    ax1.set_clip_on(False)

    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['left'].set_visible(False)
    ax1.spines['bottom'].set_color(graph_format.text_color)

    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['bottom'].set_color(graph_format.text_color)

    ax1.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='on',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    ax2.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='off',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    # Print Legends
    ax1leg = ax1.legend(loc='center left', bbox_to_anchor=(1.03, 0.53), frameon=False)
    for text in ax1leg.get_texts():
        text.set_color(graph_format.text_color)
    leg = plt.legend(loc='center left', bbox_to_anchor=(1.03, 0.47), frameon=False)
    for text in leg.get_texts():
        text.set_color(graph_format.text_color)

    plt.tight_layout()
    # plt.show()

    # Save figure
    fig_name = os.path.join(IMG_SAVE_PATH, "Inc Mitigation Time Graph" + fig_duration_str + IMG_EXTN)
    fig.savefig(fig_name, facecolor=graph_format.bgcolor, edgecolor=graph_format.edge_color,
                bbox_extra_artists=(leg,), bbox_inches='tight')


def plotSvrtyIncdntRsluTimeGraph(input_df, graph_format, title_extn=""):
    fig, ax1 = plt.subplots(num=None, figsize=(8, 5.5), dpi=80, facecolor=graph_format.bgcolor,
                            edgecolor=graph_format.edge_color)

    # Figure name postfix
    fig_duration_str = ""
    if len(title_extn) > 0:
        fig_duration_str = " - " + title_extn

    bar_width = 0.3
    opacity = 0.6

    b1 = ax1.bar(input_df.index, input_df['P3 DownTime(min)'], bar_width,
                 alpha=opacity,
                 color=graph_format.bar1_color,
                 label='P3 DownTime(min)')

    # Print column labels
    col_labels = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                                 pd.Timedelta(minutes=i).components.hours,
                                 pd.Timedelta(minutes=i).components.minutes)
                  for i in input_df['P3 DownTime(min)']]
    for rect, label in zip(b1, col_labels):
        if label == "00:00":
            continue
        height = 0.5 * rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center', va='center',
                 color=graph_format.text_color)

    b2 = ax1.bar(input_df.index, input_df['P2 DownTime(min)'], bar_width,
                 bottom=input_df['P3 DownTime(min)'],
                 alpha=opacity,
                 color=graph_format.bar2_color,
                 label='P2 DownTime(min)')

    # Print column labels
    col_labels = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                                 pd.Timedelta(minutes=i).components.hours,
                                 pd.Timedelta(minutes=i).components.minutes)
                  for i in input_df['P2 DownTime(min)']]
    base_height = [i for i in input_df['P3 DownTime(min)'].values]

    for rect, label, bh in zip(b2, col_labels, base_height):
        if label == "00:00":
            continue
        height = bh + 0.5 * rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center', va='center',
                 color=graph_format.text_color)

    b3 = ax1.bar(input_df.index, input_df['P1 DownTime(min)'], bar_width,
                 bottom=input_df['P3 DownTime(min)'] + input_df['P2 DownTime(min)'],
                 alpha=opacity,
                 color=graph_format.bar3_color,
                 label='P1 DownTime(min)')

    # Print column labels
    col_labels = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                                 pd.Timedelta(minutes=i).components.hours,
                                 pd.Timedelta(minutes=i).components.minutes)
                  for i in input_df['P1 DownTime(min)']]
    base_height = [sum(i) for i in input_df[['P2 DownTime(min)', 'P3 DownTime(min)']].values]

    for rect, label, bh in zip(b3, col_labels, base_height):
        if label == "00:00":
            continue
        height = bh + 0.5 * rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height, label, ha='center', va='center',
                 color=graph_format.text_color)

    plt.xlabel('Week Duration', fontsize=graph_format.label_fontsize, color=graph_format.text_color)
    plt.ylabel('Downtime (Hours)', fontsize=graph_format.label_fontsize, color=graph_format.text_color)

    if len(title_extn) > 0:
        title_extn = "\n" + title_extn
    ttl = plt.title('Incident Breakup & Mitigation Time/week' + title_extn,
                    color=graph_format.text_color,
                    fontsize=graph_format.title_fontsize)
    ttl.set_position([.5, 1.05])

    plt.xticks(input_df.index, input_df.Week_Duration, rotation=45, fontsize=graph_format.tick_fontsize)

    max_duration = max(input_df['DownTime(min)'])
    y_tick_increment = (((max_duration // 60) // 10) + 1) * 60
    ylables = ["%02d:%02d" % (pd.Timedelta(minutes=i).components.days * 24 +
                              pd.Timedelta(minutes=i).components.hours,
                              pd.Timedelta(minutes=i).components.minutes)
               for i in range(0, max_duration + y_tick_increment, y_tick_increment)]
    plt.yticks(np.arange(0, max_duration + y_tick_increment, y_tick_increment), ylables,
               fontsize=graph_format.tick_fontsize)

    ax2 = ax1.twinx()
    l1 = ax2.plot(input_df['P1 #Incident'], color='#cc2529', label="# of P1 Incidents",
                  alpha=1, linestyle='-', marker='s')
    l2 = ax2.plot(input_df['P2 #Incident'], color='darkgreen', label="# of P2 Incidents",
                  alpha=0.7, linestyle='--', marker='o')
    l3 = ax2.plot(input_df['P3 #Incident'], color='darkblue', label="# of P3 Incidents",
                  alpha=0.7, linestyle=':', marker='+')

    max_inc_tick_val = 5
    if max_inc_tick_val < max(input_df['#Incident']) - 1:
        max_inc_tick_val = max(input_df['#Incident']) + 1

    ax2.set_yticks(np.arange(0, max_inc_tick_val, 1))
    ax2.yaxis.label.set_color(graph_format.text_color)
    ax2.tick_params(axis='y', colors=graph_format.text_color)
    for label in ax2.xaxis.get_majorticklabels():
        label.set_fontsize(graph_format.tick_fontsize)

    ax1.yaxis.label.set_color(graph_format.text_color)
    ax1.tick_params(axis='x', colors=graph_format.text_color)
    ax1.tick_params(axis='y', colors=graph_format.text_color)
    ax1.set_facecolor(graph_format.bgcolor)
    ax1.set_clip_on(False)

    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['left'].set_visible(False)
    ax1.spines['bottom'].set_color(graph_format.text_color)

    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['bottom'].set_color(graph_format.text_color)

    ax1.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='on',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    ax2.tick_params(axis='both',  # changes apply to the both axis
                    which='both',  # both major and minor ticks are affected
                    bottom='off',
                    right='off',
                    left='off',
                    top='off')  # labels along the bottom edge are off

    leg = ax1.legend(loc='center left', bbox_to_anchor=(1.05, 0.6), frameon=False)
    for text in leg.get_texts():
        text.set_color(graph_format.text_color)
    leg = plt.legend(loc='center left', bbox_to_anchor=(1.05, 0.44), frameon=False)
    for text in leg.get_texts():
        text.set_color(graph_format.text_color)

    plt.tight_layout()
    # plt.show()

    # Save figure
    fig_name = os.path.join(IMG_SAVE_PATH, "Inc breakup Graph" + fig_duration_str + IMG_EXTN)
    fig.savefig(fig_name, facecolor=graph_format.bgcolor, edgecolor=graph_format.edge_color,
                bbox_extra_artists=(leg,), bbox_inches='tight')


def plotGraphForDates(start_date, end_date, graph_formatting, title_extn=""):
    # Generate required data
    input_data = readAndFormatSourceData(RAW_INPUT_FILE)
    uptime_data = generateUptimeWeeklyData(input_data, start_date, end_date)

    # Plot Uptime Graph
    plotUptimeGraph(uptime_data, graph_formatting, title_extn)
    plotCoreUptimeGraph(uptime_data, graph_formatting, title_extn)

    # Plot Resolution Time vs Incidents graph
    incident_data = generateIncidentWeeklyData(input_data, start_date, end_date)
    plotIncidentResolutionTimeGraph(incident_data, graph_formatting, title_extn)

    # Plot Incident Break and Resolution time/week graph
    incident_severity_data = generateIncidentSeverityWeeklyData(input_data, start_date, end_date)
    plotSvrtyIncdntRsluTimeGraph(incident_severity_data, graph_formatting, title_extn)

    return


def generateGraphsForQuarter(quarter, year, graph_formatting):
    if quarter == 1:
        qrtr_start_date = "01-01-" + str(year)
        qrtr_end_date = "03-31-" + str(year)
        title_extn = "1st Quarter 2017"
    elif quarter == 2:
        qrtr_start_date = "04-01-" + str(year)
        qrtr_end_date = "06-30-" + str(year)
        title_extn = "2nd Quarter 2017"
    elif quarter == 3:
        qrtr_start_date = "07-01-" + str(year)
        qrtr_end_date = "09-30-" + str(year)
        title_extn = "3rd Quarter 2017"
    elif quarter == 4:
        qrtr_start_date = "10-01-" + str(year)
        qrtr_end_date = "12-31-" + str(year)
        title_extn = "4th Quarter 2017"
    else:
        print("ERROR: Invalid argument. Enter correct Quarter.")
        return FAILURE

    # Check if the quarter has already started
    if datetime.strptime(qrtr_start_date, "%m-%d-%Y").date() > date.today():
        print("ERROR: Quarter has not yet started.")
        return FAILURE

    # Plot Uptime Graph for Quarter
    plotGraphForDates(qrtr_start_date, qrtr_end_date, graph_formatting, title_extn)

    return SUCCESS


def generateGraphsforMonths(month, year, graph_formatting):
    month_start_date = "%02d-01-%d" % (month, year)
    if month in [1, 3, 5, 7, 8, 10, 12]:
        month_end_date = "%02d-31-%d" % (month, year)
    elif month in [4, 6, 9, 11]:
        month_end_date = "%02d-30-%d" % (month, year)
    elif month == 2:
        month_end_date = "%02d-28-%d" % (month, year)
    else:
        print("ERROR: Invalid argument. Enter correct Month.")
        return FAILURE
    # End if

    # Check if the month has already started
    if datetime.strptime(month_start_date, "%m-%d-%Y").date() > date.today():
        print("ERROR: Month has not yet started.")
        return FAILURE

    month_name = calendar.month_name[month]
    title_extn = month_name + " " + str(year)

    # Plot Uptime Graph for Month
    plotGraphForDates(month_start_date, month_end_date, graph_formatting, title_extn)

    return SUCCESS


def exitMessage():
    print("\nHope to see you soon... ")
    print("Bye Bye")
    exit()


###############################################################################
# Main Function                                                               #
###############################################################################
QUARTERLY_GRAPH = 1
MONTHLY_GRAPH = 2
EXIT = -1
START_YEAR = 2017

if __name__ == '__main__':
    print("Welcome to RCA Graphs Reporting.\n")
    darkShade = GraphFormatting('#333333', '#f2f2f2', '#f2f2f2', 24, 14, 12, '#c0c0c0', '#778899', '#FA5A5A', "red",
                                "brown", "red")
    lightShade = GraphFormatting('#eaeaea', '#f2f2f2', 'black', 20, 14, 12, '#0066FF', 'green', 'red', "brown", "brown",
                                 "red")

    graph_type = 0
    quarter = 0
    month = 0
    year = 0
    # graph_type : 1 - Quarterly, 2 - Monthly
    while graph_type != QUARTERLY_GRAPH and graph_type != MONTHLY_GRAPH:
        print("Select the type of Graph :")
        print("\t1. Quarterly")
        print("\t2. Monthly")
        print("Enter type of graph or enter -1 to exit : ", end="")
        val = input()
        try:  # Check if the input is integer
            graph_type = int(val)
        except ValueError:
            print("\nIncorrect graph type entered [%s]. Please enter correct quarter or -1 to exit." % val)
            continue

        if graph_type == EXIT:
            exitMessage()
        elif graph_type == QUARTERLY_GRAPH or graph_type == MONTHLY_GRAPH:
            break
        else:
            print("\nIncorrect graph type entered. Please enter correct graph type.")
            # End if
    # End while

    # Get the Quarter/Month for the graphs
    if graph_type == QUARTERLY_GRAPH:
        while quarter < 1 or quarter > 4:
            print("\nEnter the Quarter of the year (1, 2, 3, 4) : ", end="")
            val = input()
            try:  # Check if the input is integer
                quarter = int(val)
            except ValueError:
                print("\nIncorrect quarter entered [%s]. Please enter correct quarter or -1 to exit." % val)
                continue

            if quarter == EXIT:
                exitMessage()
            elif quarter >= 1 and quarter <= 4:
                break
            else:
                print("\nIncorrect quarter entered. Please enter correct quarter or -1 to exit.")
                # End if
                # End While
    elif graph_type == MONTHLY_GRAPH:
        while month < 1 or month > 12:
            print("\nEnter the Month of year (1-12) : ", end="")
            val = input()
            try:  # Check if the input is integer
                month = int(val)
            except ValueError:
                print("\nIncorrect month entered [%s]. Please enter correct month or -1 to exit." % val)
                continue

            if month == EXIT:
                exitMessage()
            elif month >= 1 and month <= 12:
                break
            else:
                print("\nIncorrect month entered. Please enter correct month or -1 to exit.")
                # End if
                # End While
    # End if

    # Get the year for the graphs
    while year < START_YEAR or year > date.today().year:
        print("\nEnter the year - 2017 onwards (YYYY) : ", end="")
        val = input()
        try:  # Check if the input is integer
            year = int(val)
        except ValueError:
            print("\nIncorrect year entered [%s]. Please enter correct year or -1 to exit." % val)
            continue

        if year == -1:
            exitMessage()
        elif year >= START_YEAR and year <= date.today().year:
            break
        else:
            print("\nIncorrect year entered. Please enter correct year or -1 to exit.")
            # End if
    # End while

    retval = SUCCESS
    # Generate required graphs
    if graph_type == QUARTERLY_GRAPH:
        print("Creating graphs for Quarter %s for year %s" % (str(quarter), str(year)))
        retval = generateGraphsForQuarter(quarter, year, lightShade)
    elif graph_type == MONTHLY_GRAPH:
        print("Creating graphs for [%s %s]" % (str(calendar.month_name[month]), str(year)))
        retval = generateGraphsforMonths(month, year, lightShade)

    if retval == SUCCESS:
        print("Graphs Successfully created.")
    else:
        print("There was problem in generating graphs")
